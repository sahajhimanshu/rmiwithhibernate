package rmi.db.org;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {

	private static String DB_URL="jdbc:mysql://localhost:3306/test";
	private static String USER="root"; 
	private static String PASS="password";
      

	public static Connection getConnection() throws  SQLException {

		Connection conn = null;    

		//Register JDBC driver 
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   

		//Open a connection
		System.out.println("Connecting to a selected database..."); 
		conn = DriverManager.getConnection(DB_URL, USER, PASS); 
		System.out.println("Connected database successfully...");
		return conn;  

	}
}
