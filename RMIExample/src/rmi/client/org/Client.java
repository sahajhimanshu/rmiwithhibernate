package rmi.client.org;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.Month;
import java.util.List;

import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import org.beryx.textio.TextTerminal;

import rmi.server.org.Hello;
import rmi.server.org.Student;

public class Client {
	private Client() {}  
	public static void main(String[] args)throws Exception {  
		try { 
			TextIO textIO = TextIoFactory.getTextIO();
			String hostIP= textIO.newStringInputReader()
					.withDefaultValue("localhost")
					.read(" Host IP");
			int operation= textIO.newIntInputReader()					
					.read(" Enter Operation : 1. Add Student" +"\n"
							+ "2.Get Students Record"+"\n"
							+ "3.Update Student "+"\n"
							+ "4.Delete Student");
			Registry registry = LocateRegistry.getRegistry(hostIP,1099); 
			//Looking up the registry for the remote object 
			Hello stub = (Hello) registry.lookup("Hello");
			TextTerminal<?> terminal = textIO.getTextTerminal();
			if(operation==1){

				//				Integer student_id = textIO.newIntInputReader()
				//						.withDefaultValue(0)
				//						.read(" Student Roll Number");

				String stu_name= textIO.newStringInputReader()						
						.read(" Student Name");

				String stu_email = textIO.newStringInputReader()
						.withMinLength(8)
						//.withInputMasking(true)
						.read(" Email");

				String stu_branch= textIO.newStringInputReader()
						.withMinLength(2)					
						.read(" Branch");


				Integer student_percentage= textIO.newIntInputReader()
						.withDefaultValue(0)
						.read(" Student Percentage");



				Student st=new Student();
				//st.setID(student_id);
				st.setBranch(stu_branch);
				st.setName(stu_name);
				st.setPercent(student_percentage);
				st.setEmail(stu_email);

				stub.addUser(st);			


			}
			else if(operation == 2){

				terminal.println("Student Data "+"\n");
				List<Student> students=stub.getAllUsers();	
				for (Student student : students) {
					//TextIO.put("The square of that number is ");
					terminal.println("Student Name : "+student.getName()+"\n");
					terminal.println("Student Branch : "+student.getBranch()+"\n");
					terminal.println("Student Email : "+student.getEmail()+"\n");
					terminal.println("Student Percentage : "+student.getPercent()+"\n");
				}
				textIO.getTextTerminal().print("Thats All!");
			}
			else if(operation == 3){
				Integer student_percentage = textIO.newIntInputReader()
						.withDefaultValue(0)
						.read("Enter Student Roll Number");
				Student st=new Student();
				//				//st.setID(student_id);
				//				st.setBranch(stu_branch);
				//				st.setName(stu_name);
				st.setPercent(student_percentage);
				//				st.setEmail(stu_email);
				stub.updateUser(st);
			}				
			else if(operation == 4){
				Integer student_id = textIO.newIntInputReader()
						.withDefaultValue(0)
						.read("Enter Student Roll Number you want to delete");
				stub.deleteUser(student_id);					
			}
			else if(operation==5){
				Integer student_id = textIO.newIntInputReader()
						.withDefaultValue(0)
						.read("Enter Student Roll Number you want to delete");
				Student student=stub.getUserById(student_id);	
				terminal.println("Student Name : "+student.getName()+"\n");
				terminal.println("Student Branch : "+student.getBranch()+"\n");
				terminal.println("Student Email : "+student.getEmail()+"\n");
				terminal.println("Student Percentage : "+student.getPercent()+"\n");
			}
			else{

			}


		} catch (Exception e) { 
			System.err.println("Client exception: " + e.toString()); 
			e.printStackTrace(); 
		} 
	} 
}