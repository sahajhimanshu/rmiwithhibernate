package rmi.server.org;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server extends ImplExample {
	
	   public Server() {} 
	   public static void main(String args[]) { 
	      try {
	    	  Registry registry =null;
	         // Instantiating the implementation class 
	         ImplExample obj = new ImplExample(); 
	    
	         // Exporting the object of implementation class (
	            //here we are exporting the remote object to the stub) 
	         Hello stub = (Hello) UnicastRemoteObject.exportObject(obj, 0);  
	         
	         // Binding the remote object (stub) in the registry 
	         registry = LocateRegistry.getRegistry(); 
	         registry = LocateRegistry.createRegistry(1099);
	         registry.bind("Hello", stub);  
	         System.err.println("Server ready"); 
	      } catch (Exception e) { 
	         System.err.println("Server exception: " + e.toString()); 
	         e.printStackTrace(); 
	      } 
	   } 
	}