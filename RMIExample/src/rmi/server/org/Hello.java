package rmi.server.org;

import java.rmi.Remote;
import java.util.List;

//Creating Remote interface for our application 
public interface Hello extends Remote {  
	
	public void addUser(Student user) throws Exception;

	public void deleteUser(int userid) throws Exception;

	public void updateUser(Student user) throws Exception;

	public List<Student> getAllUsers() throws Exception;

	public Student getUserById(int userid) throws Exception;

}