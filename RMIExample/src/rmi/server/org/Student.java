package rmi.server.org;

public class Student implements java.io.Serializable {   
	/**
	 * 
	 */
	public Student() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private int studentid; 
	public int getStudentid() {
		return studentid;
	}
	public void setStudentid(int studentid) {
		this.studentid = studentid;
	}
	private int percent;   
	private String sname, branch, email;    

	 
	public String getName() { 
		return sname; 
	} 
	public String getBranch() { 
		return branch; 
	} 
	public int getPercent() { 
		return percent; 
	} 
	public String getEmail() { 
		return email; 
	} 
	
	public void setName(String name) { 
		this.sname = name; 
	} 
	public void setBranch(String branch) { 
		this.branch = branch; 
	} 
	public void setPercent(int percent) { 
		this.percent = percent; 
	} 
	public void setEmail(String email) { 
		this.email = email; 
	} 
}