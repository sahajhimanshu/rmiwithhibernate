package rmi.server.org;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import rmi.db.org.HibernateUtil;

public class ImplExample implements Hello {  
//
//	// Implementing the interface method 
//	@Override
//	public List<Student> getStudents() throws Exception {  
//		List<Student> list = new ArrayList<Student>();   
//		Statement stmt = null;  
//		//Execute a query 
//		System.out.println("Creating statement...");
//		stmt = DBConnection.getConnection().createStatement();
//		String sql = "SELECT * FROM student_data"; 
//		ResultSet rs = stmt.executeQuery(sql);  
//		//Extract data from result set 
//		while(rs.next()) { 
//			// Retrieve by column name 
//			int id  = rs.getInt("id"); 
//
//			String name = rs.getString("sname"); 
//			String branch = rs.getString("branch"); 
//
//			int percent = rs.getInt("percentage"); 
//			String email = rs.getString("email");  
//
//			// Setting the values 
//			Student student = new Student(); 
//			student.setID(id); 
//			student.setName(name); 
//			student.setBranch(branch); 
//			student.setPercent(percent); 
//			student.setEmail(email); 
//			list.add(student); 
//		} 
//		rs.close(); 
//		return list;     
//	}
//
//	@Override
//	public void insertStudents(Student student) throws Exception {
//		Statement stmt = null;  
//		// TODO Auto-generated method stub
//		System.out.println("Creating statement...");
//		stmt = DBConnection.getConnection().createStatement();
//		String sql = "insert into student_data(`id`,`sname`,`branch`,`percentage`,`email`) values('"+student.getId()+"','"+student.getName()+"','"+student.getBranch()+"','"+student.getPercent()+"','"+student.getEmail()+"')"; 
//		boolean rs = stmt.execute(sql);  
//		//Extract data from result set 
//		stmt.close(); 
//	}  
//	
	
	
	/*
	 * Using Hibernate.
	 * */
	 public void addUser(Student user) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            session.save(user);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }

	    public void deleteUser(int userid) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            Student user = (Student) session.load(Student.class, new Integer(userid));
	            session.delete(user);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }

	    public void updateUser(Student user) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            session.update(user);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }

	    public List<Student> getAllUsers() {
	        List<Student> users = new ArrayList<Student>();
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            users = session.createQuery("from Student").list();
	        } catch (RuntimeException e) {
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	        return users;
	    }

	    public Student getUserById(int userid) {
	    	Student user = null;
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            String queryString = "from Student where id = :id";
	            Query query = session.createQuery(queryString);
	            query.setInteger("id", userid);
	            user = (Student) query.uniqueResult();
	        } catch (RuntimeException e) {
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	        return user;
	    }
	}